import React from 'react';
import './taskDetail.css';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import { useSelector } from 'react-redux';

const TaskDetail = ({ open, taskInfo, onClose, onUpdate }) => {

    return (
        <Dialog
            open={open}
            onClose={onClose}
            maxWidth="md"
            aria-labelledby="form-dialog-title"
            fullWidth
        >
            <DialogTitle id="form-dialog-title">
                Task Information
            </DialogTitle>
            <DialogContent className="taskDetail">
                <label>Title:</label>
                <span>{taskInfo.title}</span>
                <br />
                <label>Description:</label>
                <span>{taskInfo.description}</span>
                <br />
                <label>Status description:</label>
                {useSelector(state => state.status.statusList)
                    .map((item, index) => {
                        if (item.statusId === taskInfo.statusId) {
                            return (
                                <span
                                    key={index}
                                    className="statusDesc"
                                >
                                    {item.title}
                                </span>
                            )
                        }

                        return null;
                    })}
                <br />
            </DialogContent>
            <DialogActions>
                <Button variant="contained" onClick={onClose} color="secondary">
                    Cancel
                </Button>
                <Button variant="contained" onClick={onUpdate} color="primary">
                    Update
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default TaskDetail;
