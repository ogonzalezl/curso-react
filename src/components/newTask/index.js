import React, { useState, useEffect } from 'react';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import TextField from '@material-ui/core/TextField';

import { useSelector } from 'react-redux';

const NewTask = props => {

    const [id, setId] = useState("");
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [statusId, setStatusId] = useState(1);
    const [invalidTitle, setInvalidTitle] = useState(true);

    useEffect(() => {
        if (props.open === true) {

            console.log("components/newTask - useEffect", "props.taskInfo", props.taskInfo);

            if (props.taskInfo.taskId) {
                setId(props.taskInfo.taskId);
                setTitle(props.taskInfo.title);
                setDescription(props.taskInfo.description);
                setStatusId(props.taskInfo.statusId);
                setInvalidTitle(false);
            } else {
                setId("");
                setTitle("");
                setDescription("");
                setStatusId(1);
                setInvalidTitle(true);
            }
        }
    }, [props.open]);

    const handleClose = event => {
        console.log("[components/newTask] - handleClose");
        props.onClose();
    };

    const handleSubmit = event => {
        console.log("[components/newTask] - handleSubmit");
        event.preventDefault();
        handleSave();
    };

    const handleSave = event => {
        console.log("[components/newTask] - handleSubmit", "taskId", id);
        console.log("[components/newTask] - handleSubmit", "title", title);
        console.log("[components/newTask] - handleSubmit", "description", description);
        console.log("[components/newTask] - handleSubmit", "statusId", statusId);

        let newTask = {
            "taskId": id,
            "title": title,
            "description": description,
            "statusId": statusId
        };

        console.log("[components/newTask] - handleSave", "newTask", newTask);

        props.onSave(newTask);
    };

    const handleChangeTitle = event => {
        console.log("[components/newTask] - handleChangeTitle", "event", event);
        setTitle(event.target.value);
        setInvalidTitle(event.target.value === "");
    };

    const handleDescription = event => {
        console.log("[components/newTask] - handleDescription", "event", event);
        setDescription(event.target.value);
    };

    const handleStatus = event => {
        console.log("[components/newTask] - handleStatus", "event", event.target);
        let id = parseInt(event.target.value, 10);
        setStatusId(id);
    };

    return (

        <Dialog
            open={props.open}
            onClose={handleClose}
            maxWidth="md"
            aria-labelledby="form-dialog-title"
            fullWidth
        >
            <DialogTitle id="form-dialog-title">
                New Task
            </DialogTitle>
            <DialogContent>
                <form onSubmit={handleSubmit} autoComplete="off">
                    <div>
                        <TextField
                            id="title"
                            label="Title"
                            value={title}
                            onChange={handleChangeTitle}
                            autoFocus
                            required
                            fullWidth
                        />
                    </div>
                    <br />
                    <div>
                        <TextField
                            id="description"
                            label="Description"
                            multiline
                            rowsMax={4}
                            value={description}
                            onChange={handleDescription}
                            fullWidth
                        />
                    </div>
                    <br />
                    <FormControl component="fieldset">
                        <FormLabel component="legend">Status</FormLabel>
                        <RadioGroup aria-label="status" name="status1" value={statusId} onChange={handleStatus}>
                            {useSelector(state => state.status.statusList)
                                .map((item, index) => {
                                    return (
                                        <FormControlLabel
                                            key={index}
                                            value={item.statusId}
                                            control={<Radio color="primary" />}
                                            label={item.title}
                                        />
                                    );
                                })
                            }
                        </RadioGroup>
                    </FormControl>
                </form>
            </DialogContent>
            <DialogActions>
                <Button variant="contained" onClick={handleClose} color="secondary">
                    Cancel
                </Button>
                <Button variant="contained" onClick={handleSave} color="primary" disabled={invalidTitle}>
                    Save
                </Button>
            </DialogActions>
        </Dialog>
    )
};

export default NewTask;