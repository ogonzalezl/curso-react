import React from 'react';
import './status.css';

import Task from '../task';

const Status = props => {

    return (
        <div className="status">
            <h2>{props.item.title}</h2>
            <div className="status-content ">
                {props.item.taskList.map((task, index) => 
                    <Task
                        key={index}
                        info={task}
                        onSelectedTask={taskInfo => props.onSelectedTask(taskInfo)}
                    />
                )}
            </div>
        </div>
    );
};

export default Status;
