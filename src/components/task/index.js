import React from 'react';
import './task.css';

const task = props =>
    <div className="task" onClick={() => props.onSelectedTask(props.info)}>
        <h4>{props.info.title}</h4>
        <div>Click for more info</div>
    </div>

export default task;