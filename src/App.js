import React, { useState, useEffect } from 'react';
import './App.css';

import { connect } from 'react-redux';

import { initStatus } from './state/actions/status';
import { addTask, updateTask } from './state/actions/task';

import ScopedCssBaseline from '@material-ui/core/ScopedCssBaseline';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';

import Status from './components/status';
import TaskDetail from './components/taskDetail';
import NewTask from './components/newTask';

import ContextApp from './context';

const App = props => {

  const { statusList, onInit, onAddTask, onUpdateTask } = props;

  const [showDetail, setShowDetail] = useState(false);
  const [showNewTask, setShowNewTask] = useState(false);

  const [taskInfo, setTaskInfo] = useState({});
  const [statusData, setStatusData] = useState([]);

  useEffect(() => {
    onInit();
  }, [])

  const handleSelectedTask = task => {
    console.log("[App] - handleSelectedTask", "task", task);

    setTaskInfo(task);
    setShowDetail(true);
  };

  const handleCloseTaskDetail = () => {
    console.log("[App] - handleCloseTaskDetail");

    setTaskInfo({});
    setShowDetail(false);
  };

  const handleUpdateTask = task => {
    console.log("[App] - handleUpdateTask");
    setShowDetail(false);
    setShowNewTask(true);
  };

  const handleSaveTask = taskInfo => {
    console.log("[App] - handleSaveTask", "taskInfo", taskInfo);

    if (taskInfo.taskId === "") {
      onAddTask(taskInfo);
    }
    else {
      onUpdateTask(taskInfo);
    }

    setTaskInfo({});
    setShowNewTask(false);
  };

  return (
    <ContextApp.Provider value={statusData}>
      <ScopedCssBaseline>
        <Container maxWidth="lg">
          <div className="actions">
            <Button
              variant="contained"
              color="primary"
              onClick={() => setShowNewTask(true)}
            >
              New Task
            </Button>
          </div>
          <div className="App">
            {statusList.map((item, index) =>
              <Status
                key={index}
                item={item}
                onSelectedTask={selectedTask => handleSelectedTask(selectedTask)}
              />
            )}

            <TaskDetail
              open={showDetail}
              taskInfo={taskInfo}
              onClose={handleCloseTaskDetail}
              onUpdate={handleUpdateTask}
            />

            <NewTask
              open={showNewTask}
              onClose={() => setShowNewTask(false)}
              onSave={handleSaveTask}
              taskInfo={taskInfo}
            />
          </div>
        </Container>
      </ScopedCssBaseline>
    </ContextApp.Provider>
  );
};

const mapStateToProps = state => {
  return {
    statusList: state.status.statusList
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onInit: () => dispatch(initStatus()),
    onAddTask: task => dispatch(addTask(task)),
    onUpdateTask: task => dispatch(updateTask(task))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
