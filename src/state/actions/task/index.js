import { ADD_TASK, UPDATE_TASK } from '../types';

export const addTask = task => {
    console.log("[actions/task] - addTask", "task", task);

    return {
        type: ADD_TASK,
        payload: {
            task: task
        }
    };
};

export const updateTask = task => {
    console.log("[actions/task] - updateTask", "task", task);

    return {
        type: UPDATE_TASK,
        payload: {
            task: task
        }
    };
};
