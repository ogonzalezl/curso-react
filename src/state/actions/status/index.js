import { INIT_STATUS_LIST } from '../types';

export const initStatus = () => {
    console.log("[actions/status] - initStatus");

    return {
        type: INIT_STATUS_LIST
    }
};
