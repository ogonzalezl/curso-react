export const INIT_STATUS_LIST = 'INIT_STATUS_LIST';

export const ADD_TASK = 'ADD_TASK';
export const UPDATE_TASK = 'UPDATE_TASK';