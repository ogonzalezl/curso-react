import { v4 as uuidv4 } from 'uuid';

const addTask = (state, data) => {
    console.log("[reducers/task] - addTask", "data", data);

    let newState = { ...state };
    let newTask = { ...data.payload.task };

    let status = newState.statusList.find(st => st.statusId === newTask.statusId);

    if (status) {
        newTask.taskId = uuidv4();
        status.taskList = [...status.taskList, newTask];
    }

    return newState;
};

export default addTask;
