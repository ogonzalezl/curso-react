const updateTask = (state, data) => {
    console.log("[reducers/task] - updateTask", "data", data);

    let newState = { ...state };
    let task = { ...data.payload.task };

    let status = newState.statusList.find(st =>
        st.taskList.filter(ta => ta.taskId === task.taskId).length > 0
    );

    if (status) {
        let taskIndex = status.taskList.findIndex(ta => ta.taskId === task.taskId);

        if (status.statusId === task.statusId) {
            status.taskList[taskIndex] = task;
        }
        else {
            status.taskList.splice(taskIndex, 1);

            let newStatus = newState.statusList.find(st => st.statusId === task.statusId);

            newStatus.taskList.push(task);
        }
    }

    console.log("[reducers/task] - updateTask", "state", state);
    console.log("[reducers/task] - updateTask", "newState", newState);
    return newState;
};

export default updateTask;
