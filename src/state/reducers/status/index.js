import { INIT_STATUS_LIST, ADD_TASK, UPDATE_TASK } from '../../actions/types';

import statusList from './statusList';
import addTask from './addTask';
import updateTask from './updateTask';

const actionHandler = {
    [INIT_STATUS_LIST]: statusList,
    [ADD_TASK]: addTask,
    [UPDATE_TASK]: updateTask
};

const initialState = {
    statusList: []
};

const reducer = (state = initialState, action = {}) => {
    const handler = actionHandler[action.type];

    return handler ? handler(state, action) : state;
};

export default reducer;