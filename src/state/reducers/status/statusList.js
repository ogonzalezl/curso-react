import statusData from '../../../data/status.json';
import taskData from '../../../data/tasks.json';

const statusList = state => {

    let statusInfo = [...statusData].map(st => {
        return {
            ...st, 
            taskList: taskData.filter(ta => ta.statusId === st.statusId)
        };
    });

    return {
        ...state,
        statusList: statusInfo
    };
};

export default statusList;